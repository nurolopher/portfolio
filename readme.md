# Introduction
Following source codes are some of the projects I worked on. Almost all of them are written only by me, some of the are written with a 2-4 developers. You can see which part I wrote which part not just by looking commit history.

## Symfony 2

**2016** (Last Commit)
- https://bitbucket.org/nturdaliev/q

**2014** (Last Commit)
- https://github.com/nurolopher/school-monitoring
- https://github.com/nurolopher/carpooling-kg

**2013** 
- https://bitbucket.org/nturdaliev/nurix

*You can find my commits from [8th page](https://bitbucket.org/nturdaliev/nurix/commits/all?page=8) of commits*. Forked from the original project where I worked with three other developers. 
### Laravel 5.1 + Angular 1.5

**2016** (Last Commit)
- https://github.com/nurolopher/findsport

### Ruby On Rails

**2014** (Last Commit)
- https://bitbucket.org/itashiev/booking/commits/branch/master


### Android

**2015** (Last Commit)
- https://github.com/nurolopher/vocabulary
  -> [Google Play Link](https://play.google.com/store/apps/details?id=com.nurolopher.toeflvocabulary)

 **2016** (Last Commit)
- https://github.com/nurolopher/Makal -> [Google Play Link](https://play.google.com/store/apps/details?id=com.nurolopher.makal)

**2015** (Last Commit)
- https://bitbucket.org/nturdaliev/table/

## Angular 2
**2016** (Last Commit)
- https://bitbucket.org/nturdaliev/angular2-fork